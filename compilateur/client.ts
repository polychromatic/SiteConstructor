///<reference path='../typings/tsd.d.ts' />

// NodeJS Dependencies :
let fs      = require('fs');
let fsx     = require('fs-extra');
let path    = require('path');
let mkdirp  = require('mkdirp');
let json    = require('jsonfile');
let q       = require('q');
let sync    = require('async');
let chalk   = require('chalk');

// Console colors!
let print   = {e:chalk.bold.red,i:chalk.bold.cyan,w:chalk.bold.yellow,o:chalk.bold.green,m:chalk.bold.magenta}

export class Client {

    public active : boolean = false;
    private upgrade : boolean = false;
    private downgrade : boolean = false;
    private upVersion : number;

    private clientDirName : string;

    // Path storage!
    public basepath : string;
    private buildpath : string;
    private creatorpath : string;
    private template_path : string;
    private template_version : number;
    private metatemplate : any;

    // Manifest storage!
    private manifest_build : any;
    private manifest_main : any;

    private plugins = {};

    private handleError = function(err) {
        console.log(print.w(err));
    }

    public init(field: string, version: string,up: number) : any {
        let deffered = q.defer();
        this.upVersion = up;
        console.log(print.i("Début de la compilation du dossier client : "));
        if(field == "noarg") {
            deffered.reject("noarg");
        }

        if(version == "--help") {
            let build_manifest = require(`../clients/${field}/creator/build.json`);
            sync.eachSeries(build_manifest.version_manifest,(v,callback) => {
                console.log(print.w("Version => ")+print.o(v));
                callback();
            },() => {
                console.log(print.w("Version => ")+print.o(build_manifest.current)+print.i(" [current]"));
                deffered.resolve("help");
            });
        }
        else {

            if(this.isDirectory(`./clients/${field}`)) {

                this.clientDirName = field;

                // Save all path
                this.basepath = `./clients/${field}`;
                this.buildpath = this.basepath+"/build";
                this.creatorpath = this.basepath+"/creator";

                // Check build directory
                this.check_field_build().then(() => {

                    if(this.isDirectory(this.creatorpath) != false) {
                        this.active = true;
                        this.manifest_build = require(`../clients/${this.clientDirName}/creator/build.json`);


                        if(version == "--down") {
                            this.downgrade = true;
                        }

                        if(version == "current" || version == this.manifest_build.current.toString()) {
                            this.manifest_main = require(`../clients/${this.clientDirName}/creator/manifest.json`);
                            deffered.resolve("Initialisation des paramètres de base OK!");
                        }
                        else if(version == "--up") {
                            this.upgrade = true;
                            this.manifest_main = require(`../clients/${this.clientDirName}/creator/manifest.json`);
                            deffered.resolve("Initialisation des paramètres de base OK! + Mise à niveau");
                        }
                        else {

                            let nVersion : any = (this.downgrade) ? up : version;
                            nVersion = nVersion.toString();

                            let find = false;
                            sync.eachSeries(this.manifest_build.version_manifest,(v,callback) => {
                                if(nVersion == v.toString()) {
                                    find = true;
                                }
                                callback();
                            },(r) => {
                                if(find) {
                                    this.manifest_main = require(`../clients/${this.clientDirName}/creator/old/manifest_${nVersion}.json`);
                                    this.upVersion = up;
                                    deffered.resolve("Chargement du manifest avec version => "+version+" sucessfull !");
                                }
                                else {
                                    this.active = false;
                                    console.log(print.i("Version disponible dans le manifest : "));
                                    sync.eachSeries(this.manifest_build.version_manifest,(v,callback) => {
                                        console.log(print.w("Version => ")+print.o(v));
                                        callback();
                                    },() => {
                                        console.log(print.w("Version => ")+print.o(this.manifest_build.current)+print.i(" [current]"));
                                        deffered.reject(new Error("Invalid version !"));
                                    });
                                }
                            });

                        }

                    }
                    else {
                        console.log("Critical Error => creator directory missing.");
                        deffered.reject(new Error("Creator directory missing! "));
                    }
                });
            }
        }
        return deffered.promise;
    }

    public build() : any {
        let deffered = q.defer();
        console.log("");
        if(this.active) {

            sync.waterfall([
                (callback) => {
                    this.loadPlugins(this.manifest_main.plugins).then(() => {
                        callback(null);
                    }).fail( this.handleError );
                },
                (callback) => {
                    let templateNFO = this.manifest_main.template;
                    if(templateNFO.name == undefined) {
                        deffered.reject(new Error("Critical Error => No template name!"));
                    }
                    this.loadTemplate(templateNFO.name,templateNFO.version || "current").then(() => {
                        callback(null);
                    }).fail( this.handleError );
                }
            ],(err,r) => {

                // Building function !
                let build = (version : number) => {
                    let wait = q.defer();
                    console.log("");
                    console.log(print.i("Création d'une build vierge - démarrage"));

                    this.manifest_build.buildID++;
                    this.buildpath += `/build_${this.manifest_build.buildID}d`;
                    json.writeFileSync(this.creatorpath+"/build.json", this.manifest_build, { spaces : 4 });

                    mkdirp(this.buildpath, (err) => {

                        if(err) console.log(err);

                        let site_manifest = {
                            creator_version : this.manifest_build.current,
                            template_version : this.template_version,
                            compilateur_version : "1.0.0"
                        }
                        json.writeFileSync(this.buildpath+"/manifest.json", site_manifest , { spaces : 4 });
                        fs.mkdirSync(this.buildpath+"/views");

                        let newTemplate_path = this.template_path.substring(1);
                        let files = ['index.html','index.css'];

                        fsx.copySync(`./templates/components.css`, this.buildpath+`/public/components.css`);
                        fsx.copySync(`./templates/gulpfile.js`, this.buildpath+`/gulpfile.js`);

                        mkdirp(this.buildpath+"/public", (err) => {
                            if (err) wait.reject(new Error("create field failed!"));
                            let extRegex = /(?:\.([^.]+))?$/;

                            sync.eachSeries(files,(o,callback) => {
                                let extension = extRegex.exec(o)[1];
                                let filePath = (extension == "html") ? "views" : "public";
                                fsx.copy(newTemplate_path+`/${o}`, this.buildpath+`/${filePath}/${o}`,(err) => {
                                    if(err) console.log(err);
                                    callback();
                                });
                            },() => {
                                console.log(print.o("   => terminé"));
                                wait.resolve("Ok");
                            });
                        });

                    });
                    return wait.promise;
                }

                let final = (new_idBuild) => {
                    build(new_idBuild)
                    .then((res) => {
                        deffered.resolve({
                            main_manifest : this.manifest_main,
                            build_manifest : this.manifest_build,
                            build_path : this.buildpath,
                            meta_template : this.metatemplate
                        });
                    })
                    .fail((err) => {
                        deffered.reject(err);
                    });
                }

                if(this.upgrade) {
                    console.log(print.i("---> Upgrade du projet <---"));
                    let currentID = this.manifest_build.current;
                    let old_idBuild = currentID.toString();
                    let new_idBuild = (this.upVersion >= 1) ? Math.floor(currentID + this.upVersion) : currentID + this.upVersion;

                    this.manifest_build.version_manifest.push(currentID);
                    this.manifest_build.current = new_idBuild;

                    json.writeFileSync(this.creatorpath+"/build.json", this.manifest_build, { spaces : 4 });
                    fsx.copySync(this.creatorpath+"/manifest.json", this.creatorpath+`/old/manifest_${old_idBuild}.json`);

                    final(new_idBuild);
                }
                else if(this.downgrade) {
                    console.log(print.i("---> Downgrade du projet <---"));
                    let index = this.manifest_build.version_manifest.indexOf(this.upVersion);
                    let versions = this.manifest_build.version_manifest;
                    let old_indexs = versions.splice(index);

                    sync.eachSeries(old_indexs,(versionID,callback) => {
                        fs.unlink(this.creatorpath+`/old/manifest_${versionID}.json`,(err) => {
                            if(err) {
                                console.log(err);
                            }
                            callback();
                        });
                    },() => {
                        console.log(print.o("---> Downgrade success <---"));
                        this.manifest_build.current = this.upVersion;
                        this.manifest_build.version_manifest.splice(index);
                        json.writeFileSync(this.creatorpath+"/build.json", this.manifest_build, { spaces : 4 });
                        final(this.upVersion);
                    });
                }
                else {
                    final(this.manifest_build.current);
                }
            });


        }
        else {
            deffered.reject(new Error("Build not active!"));
        }
        return deffered.promise;
    }

    private loadPlugins(list : Object) : any {
        let deffered = q.defer();
        console.log(print.i("Chargement des plugins : "))
        sync.forEachOf(list,(v,k,callback) => {
            if(this.isDirectory(`./plugins/${k}`)) {
                console.log(print.m(`   => ${k} `)+print.o("=> OK"));
                this.plugins[k] = {
                    directory : `../plugins/${k}`,
                    param : v
                };
                callback();
            }
            else {
                console.log(print.m(`   => ${k} `)+print.e("=> BAD"));
                callback();
            }
        }, () => {
            deffered.resolve("ok");
        });
        return deffered.promise;
    }

    private loadTemplate(name: string,version: string) : any {
        let deffered = q.defer();
        console.log("");
        console.log(print.i("Chargement du template : "));

        // Déclaration de la stack
        let stack = (Opath : string) => {
            let stack_wait = q.defer();
            let build_manifest : any = require(Opath+"/build.json");

            let findVersion = (chemin: string) => {
                let wait = q.defer();
                if(version == "current" || version == build_manifest.current) {
                    this.template_version = build_manifest.current;
                    wait.resolve(Opath+`/${build_manifest.current}`);
                }
                else {
                    sync.eachSeries(build_manifest.versions,(item,callback) => {
                        if(item == version) {
                            this.template_version = parseFloat(version);
                            callback({ path : Opath+`/${item}`});
                        }
                        else {
                            callback();
                        }
                    },(result) => {
                        if(result != undefined) {
                            return wait.resolve(result.path);
                        }
                        else {
                            console.log(print.w("Impossible de trouver la version => "+version));
                            console.log(print.o("Remplacement par la version de base !"));
                            wait.resolve(path+`/${build_manifest.current}`);
                        }
                    });
                }
                return wait.promise;
            }

            findVersion(path).then( (version_path) => {
                if(this.isDirectory(version_path.substring(1))) {
                    stack_wait.resolve(version_path);
                }
                else {
                    stack_wait.reject(new Error("Impossible de trouver le dossier du template!"));
                }
            }).fail((err) => {
                console.log(err);
            });

            return stack_wait.promise;
        }

        // On configure un failKiller
        let killHandler = (err) => {
            console.log(print.w(err));
            this.active = false;
            deffered.reject(new Error("Impossible de terminé la compilation!"));
        }

        if(this.isDirectory(`./templates/${name}`) !== false) {
            console.log(print.m("   => Template de type 'classique' trouvé"));
            stack(`../templates/${name}`)
            .then((res) => {
                console.log(print.o("   Template => ")+print.w(name)+print.o(" trouver et charger avec succèss"));
                this.template_path = res;
                this.metatemplate = this.template_path+"/manifest.json";
                deffered.resolve();
            })
            .fail(killHandler);
        }
        else {
            if(this.isDirectory(`./clients/${this.clientDirName}/templates/${name}`) !== false) {
                console.log(print.m("   => Template de type 'sur-mesure' trouvé"));
                stack(`../clients/${this.clientDirName}/templates/${name}`)
                .then((res) => {
                    console.log(print.o("   Template => ")+print.w(name)+print.o(" trouver et charger avec succèss"));
                    this.template_path = res;
                    this.metatemplate = this.template_path+"/manifest.json";
                    deffered.resolve();
                })
                .fail(killHandler);
            }
            else {
                deffered.reject(new Error("Impossible de trouver le template en question!"));
            }
        }
        return deffered.promise;
    }

    // Verify if build field is here!
    private check_field_build() : any {
        let deffered = q.defer();
        if(this.isDirectory(this.buildpath) === false) {
            mkdirp(this.buildpath, function(err) {
                if(err)
                    console.log(print.w(err));
                deffered.resolve(true);
            });
        }
        else {
            deffered.resolve(true);
        }
        return deffered.promise;
    }

    private rmDir(dirPath : string) : any {
        let files;
        try {
            files = fs.readdirSync(dirPath);
        }
        catch(e) {
            return false;
        }
        if (files.length > 0)
            for (let i = 0; i < files.length; i++) {
                let filePath = dirPath + '/' + files[i];
                if (fs.statSync(filePath).isFile())
                    fs.unlinkSync(filePath);
                else
                    this.rmDir(filePath);
            }
        return true;
    };

    // Check directory function
    private isDirectory(path : string) : boolean {
        try {
            if(fs.realpathSync(path)) {
                return true;
            }
        }
        catch(e)
        {
            return false;
        };
    }

    private getDirectoryFrom(srcpath : string) {
        return fs.readdirSync(srcpath).filter( (file) => {
            return fs.statSync( path.join(srcpath, file) ).isDirectory();
        });
    }
}
