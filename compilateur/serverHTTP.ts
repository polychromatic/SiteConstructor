///<reference path='../typings/tsd.d.ts' />

// Require NodeJS dependencies
let q       = require('q');
let sync    = require('async');
let fs      = require('fs');
let fsx     = require('fs-extra');
let path    = require('path');
let mkdirp  = require('mkdirp');
let jsdom   = require("jsdom");

let chalk = require('chalk');
let print = {e:chalk.bold.red,i:chalk.bold.cyan,w:chalk.bold.yellow,o:chalk.bold.green,m:chalk.bold.magenta}

export class serverHTTP {

    private final : string = "";
    private header : string = "";
    private routing : string = "";

    private routes = [];
    private options;

    private build_path : string;

    constructor(buildPath: string,options?: any) {
        options             = options || {};
        this.routes         = options.routing || [];
        options.port        = options.port || "2500";
        options.ip          = options.ip || "0.0.0.0";
        this.options        = options;

        this.build_path     = buildPath;
        console.log("");
        console.log("<--------------------------------------->");
        console.log(print.o("Construction du middleware javascript : "));
        console.log("<--------------------------------------->");
        console.log("");
    }

    private makeRouting() : any {
        let deffered = q.defer();
        sync.eachSeries(this.routes,(o,callback)=> {
            let newO = o.substring(1);
            let firstLetter = o.charAt(0);

            if(firstLetter != "/") {
                console.log(print.e("   => Routing incorrect!"));
                callback();
            }
            else {
                if(o == "/") {
                    this.addRoute('/','index');
                    callback();
                }
                else {
                    this.addRoute(o.charAt(0),`${newO}`);
                    callback();
                }
            }
        },()=> {
            deffered.resolve();
        });
        return deffered.promise;
    }

    private addRoute(link: string,fileName: string) : void {
        this.routing += `app.get('${link}', function (req, res) {res.render('${fileName}');});`
    }

    public generate() : any {
        let deffered = q.defer();
        let handleError = function(err) {
            console.log(err);
        }
        sync.waterfall([
            (callback) => {
                this.makeRouting().then((res)=> {
                    callback();
                }).fail( handleError );
            }
        ],
        (err,r) => {
            this.header = `var express=require("express"),app=express(),path=require("path");app.use(express.static(path.join(__dirname,"/public"))),app.set("views",path.join(__dirname,"views")),app.set("view engine","jade");var server=app.listen(2500,"0.0.0.0");`;
            this.final = this.header + this.routing;
            fs.writeFile( this.build_path+"/server.js" , this.final , function (err) {
                if (err) deffered.reject(new Error("Echec de l'écriture du server.js"));
                else deffered.resolve("  => Server construit avec succès!");
            });
        });

        return deffered.promise;
    }
}
