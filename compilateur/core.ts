///<reference path='../typings/tsd.d.ts' />

// Require NodeJS dependencies
let q       = require('q');
let sync    = require('async');
let fs      = require('fs');
let fsx     = require('fs-extra');
let path    = require('path');
let mkdirp  = require('mkdirp');
let jsdom   = require("jsdom");

let chalk = require('chalk');
let print = {e:chalk.bold.red,i:chalk.bold.cyan,w:chalk.bold.yellow,o:chalk.bold.green,m:chalk.bold.magenta}

export class Compilateur {

    private manifest : any;
    private meta : any;
    private build_path : string;

    private object_Styles = {};
    private object_Store = {};

    public routes = [];
    public html : string;

    constructor(manifest:any,meta:any,build_path: string) {
        this.manifest = manifest.site;
        this.meta = require(meta).meta;
        this.build_path = build_path;
        console.log("<--------------------------------------->");
        console.log(print.o("Construction de la vue : "));
        console.log("<--------------------------------------->");
        console.log("");
    }

    public generate() : any {
        let deffered = q.defer();
        let errorHandler = (err) => {
            console.log(err);
        }
        sync.waterfall([
            (callback) => {
                this.make().then( (msg) => {
                    callback();
                }).fail( errorHandler );
            },
            (callback) => {
                this.loadView().then( (msg) => {
                    callback();
                }).fail( errorHandler );
            },
            (callback) => {
                this.applyView().then( (msg) => {
                    callback();
                }).fail( errorHandler );
            },
            (callback) => {
                this.applyStyle().then( (html) => {
                    callback(null, html);
                }).fail( errorHandler );
            }
        ],(err,r) => {
            this.saveView(r).then((msg) => {
                deffered.resolve(msg);
            });
        });
        return deffered.promise;
    }

    public make() : any {
        let deffered = q.defer();
        sync.forEachOf(this.manifest,(v,k,callback) => {
            this.routes.push(k);
            this.recursiveObjects(v,true)
            .then((res) => {
                callback();
            })
            .fail((err) => {
                console.log(err);
            });
        }, (err) => {
            deffered.resolve("Make done!");
        });
        return deffered.promise;
    }

    public applyView() : any {
        let deffered = q.defer();
        console.log(print.i("Construction de l'html - début"));

        jsdom.env(this.html,["http://code.jquery.com/jquery-2.1.4.min.js"], (errors, window) => {
            let $ = window.$;
            sync.forEachOf(this.object_Store, (v,k,callback) =>{
                let parent = v.parent;
                if(parent != null) {
                    let o = $('#'+parent);
                    if(o.length != 0) {
                        o.append(`<div id="${k}"></div>`);
                    }
                    callback();
                }
                else {
                    callback();
                }
            }, (err) => {
                console.log(print.o("   => Terminé"));
                this.html = window.document.documentElement.innerHTML;
                deffered.resolve(this.html);
            });
        });

        return deffered.promise;
    }

    public loadView() : any {
        let deffered = q.defer();
        fs.readFile( this.build_path+"/views/index.html", 'utf8', (err, html) =>{
            if(err) deffered.reject(new Error("Impossible de lire le fichier .html"));
            this.html = html;
            console.log(print.o("Chargement du fichier .html OK!"));
            console.log("");
            deffered.resolve(true);
        });
        return deffered.promise;
    }

    public saveView(html: string) : any {
        console.log("");
        console.log(print.w("Réecriture du fichier html - début"));
        let deffered = q.defer();
        fs.writeFile( this.build_path+"/views/index.html" , html, function (err) {
            if (err)
                deffered.reject(new Error("Echec de la réecriture du fichier html."));
            else
                this.html = html;
                deffered.resolve("  => Terminé");
        });
        return deffered.promise;
    }

    public applyStyle() : any {
        let deffered = q.defer();
        console.log("");
        console.log(print.i("Application du style dans l'html - début"));

        jsdom.env(this.html,["http://code.jquery.com/jquery-2.1.4.min.js"], (errors, window) => {
            let $ = window.$;
            $('.jsdom:odd').remove();
            sync.forEachOf(this.object_Styles, (v,k,callback) =>{
                if(Object.keys(v).length > 0)
                    sync.forEachOf(v, (i,x,go) =>{
                        let o = $('#'+k);
                        let newI = i.toString();
                        if(o.length != 0) {
                            if(x == "type")
                                o.addClass(newI);
                            else if(x == "content")
                                o.html(newI);
                            else
                                o.css(x,newI);
                        }
                        go();
                    }, (err) => {
                        callback();
                    });
            }, (err) => {
                console.log(print.o("   => Terminé"));
                this.html = window.document.documentElement.innerHTML;
                deffered.resolve(this.html);
            });
        });

        return deffered.promise;
    }

    public recursiveObjects(o: any,lock: boolean,name?:string) : any {
        let deffered = q.defer();
        sync.forEachOf(o,(v,k,callback) => {
            if(typeof v != "object" && !lock) {
                if(this.object_Styles[name] == undefined)
                    this.object_Styles[name] = {};
                this.object_Styles[name][k] = v;
                callback();
            }
            else {
                if(Object.keys(v).length > 0) {
                    this.object_Store[k] = (name) ? {parent:name} : {parent:null};
                    this.recursiveObjects(v,false,k)
                    .then((res) => {
                        callback();
                    })
                    .fail((err) => {
                        console.log(err);
                    });
                }
                else {
                    callback();
                }
            }

        }, (err) => {
            if(err) console.log(err);
            deffered.resolve();
        });
        return deffered.promise;
    }
}
