# Road map

- [x] Ajouter une notion de "composant".
- [x] Ajouter des paramètres au plugin (dans le manifest.json).
- [ ] Développement d'un plugin (versionning + infrastructure).
- [ ] Améliorer les composants !
- [x] Ajouter la construction du middleware Express.
- [ ] Ajouter la gestion multi-page (avec rendering static au début).

- [x] Ajouter gulp (uglification JS + CSS).
- [ ] Ajouter du routing Angular propre!
