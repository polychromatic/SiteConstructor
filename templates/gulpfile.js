// Require gulp package
var gulp            = require('gulp');
var uglify          = require('gulp-uglify');
var autoprefixer    = require('gulp-autoprefixer');
var minifyCss       = require('gulp-minify-css');
var minifyHTML      = require('gulp-minify-html');
var html2jade = require('gulp-html2jade');

gulp.task('js', function() {
    return gulp.src('server.js', { base : "./" })
        .pipe(uglify())
        .pipe(gulp.dest('.'));
});

gulp.task('css', function () {
    return gulp.src('./public/*.css')
        .pipe(autoprefixer())
        .pipe(minifyCss({compatibility: 'ie10'}))
        .pipe(gulp.dest('./public'));
});

gulp.task('minify-html', function() {
    return gulp.src('./views/*.html')
        .pipe(minifyHTML({conditionals : true,spare :true}))
        .pipe(gulp.dest('./views'));
});

gulp.task('html', function(){
    return gulp.src('./views/*.html')
        .pipe(html2jade({nspaces:2}))
        .pipe(gulp.dest('./views'));
});

gulp.task('default',['js','css','html']);
