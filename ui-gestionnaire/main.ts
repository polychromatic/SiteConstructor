///<reference path='../typings/tsd.d.ts' />

// Configuration de l'ui
let configuration = require('../configs/ui.json');

// Require NodeJS dependencies
let path        = require('path');

// Express et Configuration.json
let express     = require('express');
let app         = express();
let server      = app.listen(configuration.port,configuration.ip);

// Require express dependencies
let bodyparser  = require("body-parser");

// Configure express sample
//app.use(express.static( path.join(__dirname,'../public') ));
app.set('views',path.join(__dirname,'../views'));
app.set('view engine','jade');

app.get('/',function(req,res) {
    res.render("layout");
});
