///<reference path='typings/tsd.d.ts' />

// Insertion des dépendances NodeJS :
let fs      = require('fs');
let fsx     = require('fs-extra');
let path    = require('path');
let mkdirp  = require('mkdirp');
let jsdom   = require("jsdom");
let q       = require('q');
let sync    = require('async');
let chalk   = require('chalk');
var exec    = require('child_process').exec;
let print   = {e:chalk.bold.red,i:chalk.bold.cyan,w:chalk.bold.yellow,o:chalk.bold.green,m:chalk.bold.magenta}

// On import les fichiers de configuration.
let build   = require(`./configs/build.json`);

// On importe la class compilateur au créateur.
import {Compilateur} from "./compilateur/core";
import {Client} from "./compilateur/client";
import {serverHTTP} from "./compilateur/serverHTTP";

if(build != undefined) {
    // On récupère les arguments NodeJS
    let arg_client  = process.argv[2] || "noarg";
    let arg_version = process.argv[3] || "current";
    let arg_up      = (process.argv[4]) ? parseFloat(process.argv[4]) : 1;
    let handleError = (err) => {console.log(print.e(err));}

    // On initialise notre client !
    let client = new Client();

    client.init(arg_client,arg_version,arg_up).then( (res) => {
        if(res == "help") {
            process.exit();
        }
        console.log(print.w(res));
        client.build().then( (info) => {
            let compilateur = new Compilateur(info.main_manifest,info.meta_template,info.build_path);
            compilateur.generate().then((msg) => {
                console.log(print.o(msg));

                let HTTP = new serverHTTP(info.build_path, {
                    routing : compilateur.routes,
                    port : "2500",
                    ip : "0.0.0.0"
                });
                HTTP.generate().then( (msg) => {
                    console.log(print.o(msg));
                    console.log("");
                    console.log(print.i("Gulpification de la build - démarrage"))
                    var parentDir = path.resolve(process.cwd(), info.build_path);
                    exec("gulp",{cwd: parentDir},(error, stdout, stderr)=>{
                        console.log("");
                        console.log(print.o("   => Terminé"));
                    })
                }).fail( handleError );

            }).fail(handleError);
        }).fail( handleError )
    }).fail( handleError );
}
else {
    console.log(print.i("Configuration de la build introuvable."));
    process.exit();
}
